<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * BillVariables Controller
 *
 * @property \App\Model\Table\BillVariablesTable $BillVariables
 */
class BillVariablesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $billVariables = $this->paginate($this->BillVariables);

        $this->set(compact('billVariables'));
        $this->set('_serialize', ['billVariables']);
    }

    /**
     * View method
     *
     * @param string|null $id Bill Variable id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $billVariable = $this->BillVariables->get($id, [
            'contain' => []
        ]);

        $this->set('billVariable', $billVariable);
        $this->set('_serialize', ['billVariable']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $billVariable = $this->BillVariables->newEntity();
        if ($this->request->is('post')) {
            $billVariable = $this->BillVariables->patchEntity($billVariable, $this->request->data);
            if ($this->BillVariables->save($billVariable)) {
                $this->Flash->success(__('The bill variable has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The bill variable could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('billVariable'));
        $this->set('_serialize', ['billVariable']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Bill Variable id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $billVariable = $this->BillVariables->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $billVariable = $this->BillVariables->patchEntity($billVariable, $this->request->data);
            if ($this->BillVariables->save($billVariable)) {
                $this->Flash->success(__('The bill variable has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The bill variable could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('billVariable'));
        $this->set('_serialize', ['billVariable']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Bill Variable id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $billVariable = $this->BillVariables->get($id);
        if ($this->BillVariables->delete($billVariable)) {
            $this->Flash->success(__('The bill variable has been deleted.'));
        } else {
            $this->Flash->error(__('The bill variable could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
