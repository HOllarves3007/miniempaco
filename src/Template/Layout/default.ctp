<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $title_for_layout?></title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <!-- Include external files and scripts here (See HTML helper for more info.) -->
    <?php
            echo $this->fetch('meta');
            echo $this->Html->css('base');
            echo $this->Html->css('cake');
            echo $this->Html->css('bootstrap.min');
            echo $this->Html->css('grayscale');
            echo $this->Html->css('font-awesome.min');
            echo $this->Html->css('style');
            ?>
</head>
<body>

<!-- If you'd like some sort of menu to
show up on all of your views, include it here -->
<div id="header">
</div>

<!-- Here's where I want my views to be displayed -->
<?php echo $this->fetch('content'); ?>

<!-- Add a footer to each displayed page -->
<div id="footer">
    <?php
            echo $this->Html->script('jquery'); // Include jQuery library
            echo $this->Html->script('bootstrap.min');
            echo $this->Html->script('jquery.easing.min');
            echo $this->Html->script('grayscale');
    ?>
</div>

</body>
</html>