<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $bill->billId],
                ['confirm' => __('Are you sure you want to delete # {0}?', $bill->billId)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Bills'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="bills form large-9 medium-8 columns content">
    <?= $this->Form->create($bill) ?>
    <fieldset>
        <legend><?= __('Edit Bill') ?></legend>
        <?php
            echo $this->Form->input('billNumber');
            echo $this->Form->input('kRecieved');
            echo $this->Form->input('productionPrct');
            echo $this->Form->input('kProduced');
            echo $this->Form->input('productionCost');
            echo $this->Form->input('totalRawCost');
            echo $this->Form->input('salePrice');
            echo $this->Form->input('date', ['empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
