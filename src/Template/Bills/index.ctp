<nav class="large-3 medium-4 columns action-sidebar" id="actions-sidebar">
    <div class="side-navigation navbar-brand">
        <a class="page-scroll clearfix" href="#page-top">
            <i class="fa fa-plus pull-left"></i>
            <span class="pull-right"><?= $this->Html->link(__('Nueva Factura'), ['action' => 'add']) ?></span>
        </a>
    </div>
</nav>
<div class="bills index large-9 medium-8 columns content">
    <a class="navbar-brand page-scroll" href="#page-top">
        <i class="fa fa-play-circle playCircle"></i><span class="light facturaTitle">Facturas</span>
    </a>
    <table cellpadding="0" cellspacing="0" class="billTable">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('billId','Id de Factura') ?></th>
                <th><?= $this->Paginator->sort('billNumber', 'Nro. de Factura') ?></th>
                <th><?= $this->Paginator->sort('kRecieved', 'Kilos Recibidos') ?></th>
                <th><?= $this->Paginator->sort('kProduced', 'Kilos Producidos') ?></th>
                <th><?= $this->Paginator->sort('productionCost', 'Costos de Producción') ?></th>
                <th><?= $this->Paginator->sort('totalRawCost', 'Costos de Materia Prima') ?></th>
                <th class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($bills as $bill): ?>
            <tr>
                <td><?= $this->Number->format($bill->billId) ?></td>
                <td><?= $this->Number->format($bill->billNumber) ?></td>
                <td><?= $this->Number->format($bill->kRecieved) ?></td>
                <td><?= $this->Number->format($bill->productionPrct) ?></td>
                <td><?= $this->Number->format($bill->kProduced) ?></td>
                <td><?= $this->Number->format($bill->productionCost) ?></td>
                <td><?= $this->Number->format($bill->totalRawCost) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $bill->billId]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $bill->billId]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $bill->billId], ['confirm' => __('Are you sure you want to delete # {0}?', $bill->billId)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="bottom paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('Antes')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Despues') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
