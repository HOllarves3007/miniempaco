<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Bill'), ['action' => 'edit', $bill->billId]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Bill'), ['action' => 'delete', $bill->billId], ['confirm' => __('Are you sure you want to delete # {0}?', $bill->billId)]) ?> </li>
        <li><?= $this->Html->link(__('List Bills'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Bill'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="bills view large-9 medium-8 columns content">
    <h3><?= h($bill->billId) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('BillId') ?></th>
            <td><?= $this->Number->format($bill->billId) ?></td>
        </tr>
        <tr>
            <th><?= __('BillNumber') ?></th>
            <td><?= $this->Number->format($bill->billNumber) ?></td>
        </tr>
        <tr>
            <th><?= __('KRecieved') ?></th>
            <td><?= $this->Number->format($bill->kRecieved) ?></td>
        </tr>
        <tr>
            <th><?= __('ProductionPrct') ?></th>
            <td><?= $this->Number->format($bill->productionPrct) ?></td>
        </tr>
        <tr>
            <th><?= __('KProduced') ?></th>
            <td><?= $this->Number->format($bill->kProduced) ?></td>
        </tr>
        <tr>
            <th><?= __('ProductionCost') ?></th>
            <td><?= $this->Number->format($bill->productionCost) ?></td>
        </tr>
        <tr>
            <th><?= __('TotalRawCost') ?></th>
            <td><?= $this->Number->format($bill->totalRawCost) ?></td>
        </tr>
        <tr>
            <th><?= __('SalePrice') ?></th>
            <td><?= $this->Number->format($bill->salePrice) ?></td>
        </tr>
        <tr>
            <th><?= __('Date') ?></th>
            <td><?= h($bill->date) ?></td>
        </tr>
    </table>
</div>
