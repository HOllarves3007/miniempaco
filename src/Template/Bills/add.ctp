<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <div class="side-navigation navbar-brand">
        <a class="page-scroll clearfix" href="#page-top">
            <i class="fa fa-plus pull-left"></i>
            <span class="pull-right"><?= $this->Html->link(__('Lista de Facturas'), ['action' => 'index']) ?></span>
        </a>
    </div>
</nav>
<div class="bills form large-9 medium-8 columns content">
    <div class="container">
        <div class="row">
            <?= $this->Form->create($bill)?>
            <div class="col-lg-6">
                <div class="billFormInput">
                    <?php
                        echo $this->Form->inputs(['billNumber' => [ 'legend' => false,
                                                                'label' => 'Nro. de Factura'    ]]);
                    ?>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="billFormInput">
                    <?php
                    echo $this->Form->inputs(['kRecieved' => [  'label' => 'Kilos Recibidos',
                                                                'legend' => false             ]]);
                    ?>
                </div>
            </div>
            <div class="billFormDate">
                <?php
                    echo $this->Form->input('date', ['empty' => true]);
                ?>
            </div>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
