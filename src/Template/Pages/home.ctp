<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <!-- Include external files and scripts here (See HTML helper for more info.) -->
    <?php
            echo $this->fetch('meta');
            echo $this->Html->css('base');
            echo $this->Html->css('bootstrap.min');
            echo $this->Html->css('grayscale');
            echo $this->Html->css('font-awesome.min');
            echo $this->Html->css('style');



            echo $this->Html->script('jquery'); // Include jQuery library
            echo $this->Html->script('bootstrap.min');
            echo $this->Html->script('jquery.easing.min');
            echo $this->Html->script('grayscale');
            ?>
</head>
<body>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Navigation -->
<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">
                <i class="fa fa-play-circle"></i>  <span class="light">Empaco</span> Avicola
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
            <ul class="nav navbar-nav">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li>
                    <a class="page-scroll" href="#about">Registros</a>
                </li>
                <li>
                    <a class="page-scroll" href="#download">Totales</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
<!-- Intro Header -->
<header class="intro empacoHeading">
    <div class="intro-body">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="brand-heading text-center">G3</h1>
                    <a href="#bill" class="btn btn-circle page-scroll">
                        <i class="fa fa-angle-double-down animated"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Choice Section -->
<section id="bill" class="content-section text-center">
    <div class="bill-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Registros</h2>
                </div>
                <div class="col-md-6 text-center">
                    <button class="btn billButtons"><?php echo $this->Html->link(
                        'Registrar Factura',
                        '/bills/index',
                        ['target' => 'self']
                        ); ?></button>
                </div>
                <div class="col-md-6 text-center">
                    <button class="btn billButtons"><?php echo $this->Html->link(
                            'Totales',
                            '/total/index',
                            ['target' => 'self']
                            ); ?>
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo $this->fetch('content'); ?>

<!-- Add a footer to each displayed page -->
<div id="footer"></div>

</body>
</html>