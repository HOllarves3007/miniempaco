<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Bill Variable'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="billVariables index large-9 medium-8 columns content">
    <h3><?= __('Bill Variables') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('billVariableId') ?></th>
                <th><?= $this->Paginator->sort('billId') ?></th>
                <th><?= $this->Paginator->sort('rawCost') ?></th>
                <th><?= $this->Paginator->sort('fletCost') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($billVariables as $billVariable): ?>
            <tr>
                <td><?= $this->Number->format($billVariable->billVariableId) ?></td>
                <td><?= $this->Number->format($billVariable->billId) ?></td>
                <td><?= $this->Number->format($billVariable->rawCost) ?></td>
                <td><?= $this->Number->format($billVariable->fletCost) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $billVariable->billVariableId]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $billVariable->billVariableId]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $billVariable->billVariableId], ['confirm' => __('Are you sure you want to delete # {0}?', $billVariable->billVariableId)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
