<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Bill Variable'), ['action' => 'edit', $billVariable->billVariableId]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Bill Variable'), ['action' => 'delete', $billVariable->billVariableId], ['confirm' => __('Are you sure you want to delete # {0}?', $billVariable->billVariableId)]) ?> </li>
        <li><?= $this->Html->link(__('List Bill Variables'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Bill Variable'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="billVariables view large-9 medium-8 columns content">
    <h3><?= h($billVariable->billVariableId) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('BillVariableId') ?></th>
            <td><?= $this->Number->format($billVariable->billVariableId) ?></td>
        </tr>
        <tr>
            <th><?= __('BillId') ?></th>
            <td><?= $this->Number->format($billVariable->billId) ?></td>
        </tr>
        <tr>
            <th><?= __('RawCost') ?></th>
            <td><?= $this->Number->format($billVariable->rawCost) ?></td>
        </tr>
        <tr>
            <th><?= __('FletCost') ?></th>
            <td><?= $this->Number->format($billVariable->fletCost) ?></td>
        </tr>
    </table>
</div>
