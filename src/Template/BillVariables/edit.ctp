<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $billVariable->billVariableId],
                ['confirm' => __('Are you sure you want to delete # {0}?', $billVariable->billVariableId)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Bill Variables'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="billVariables form large-9 medium-8 columns content">
    <?= $this->Form->create($billVariable) ?>
    <fieldset>
        <legend><?= __('Edit Bill Variable') ?></legend>
        <?php
            echo $this->Form->input('billId');
            echo $this->Form->input('rawCost');
            echo $this->Form->input('fletCost');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
