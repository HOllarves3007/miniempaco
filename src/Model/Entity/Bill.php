<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Bill Entity.
 *
 * @property int $billId
 * @property int $billNumber
 * @property float $kRecieved
 * @property float $productionPrct
 * @property float $kProduced
 * @property float $productionCost
 * @property float $totalRawCost
 * @property float $salePrice
 * @property \Cake\I18n\Time $date
 */
class Bill extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'billId' => false,
    ];
}
