<?php
namespace App\Model\Table;

use App\Model\Entity\BillVariable;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BillVariables Model
 *
 */
class BillVariablesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('bill_variables');
        $this->displayField('billVariableId');
        $this->primaryKey('billVariableId');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('billVariableId')
            ->allowEmpty('billVariableId', 'create');

        $validator
            ->integer('billId')
            ->requirePresence('billId', 'create')
            ->notEmpty('billId');

        $validator
            ->decimal('rawCost')
            ->allowEmpty('rawCost');

        $validator
            ->decimal('fletCost')
            ->allowEmpty('fletCost');

        return $validator;
    }
}
