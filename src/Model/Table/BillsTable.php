<?php
namespace App\Model\Table;

use App\Model\Entity\Bill;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Bills Model
 *
 */
class BillsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('bills');
        $this->displayField('billId');
        $this->primaryKey('billId');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('billId')
            ->allowEmpty('billId', 'create');

        $validator
            ->integer('billNumber')
            ->allowEmpty('billNumber');

        $validator
            ->decimal('kRecieved')
            ->allowEmpty('kRecieved');

        $validator
            ->decimal('productionPrct')
            ->allowEmpty('productionPrct');

        $validator
            ->decimal('kProduced')
            ->allowEmpty('kProduced');

        $validator
            ->decimal('productionCost')
            ->allowEmpty('productionCost');

        $validator
            ->decimal('totalRawCost')
            ->allowEmpty('totalRawCost');

        $validator
            ->decimal('salePrice')
            ->allowEmpty('salePrice');

        $validator
            ->date('date')
            ->allowEmpty('date');

        return $validator;
    }
}
