<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BillVariablesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BillVariablesTable Test Case
 */
class BillVariablesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BillVariablesTable
     */
    public $BillVariables;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bill_variables'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BillVariables') ? [] : ['className' => 'App\Model\Table\BillVariablesTable'];
        $this->BillVariables = TableRegistry::get('BillVariables', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BillVariables);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
